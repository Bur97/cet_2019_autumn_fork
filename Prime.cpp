/*На вход программы вводится целое число n. Программа выводит первые n простых
чисел, начиная с 2, если n - положительное. Если же n - отрицательно, то программа
также выводит первые -n чисел, но в обратном порядке, т.е. 2 - последнее в списке.
4   ->  2 3 5 7
-4  ->  7 5 3 2*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

/*Функция LeastPrimeDivisor(n, i) возвращает наименьший делитель числа n,
который не меньше заданного числа i, т.е. 
LeastPrimeDivisor(4, 2) = 2, LeastPrimeDivisor(4, 3) = 4*/

int LeastPrimeDivisor(int n, int i)
{
    if (i*i > n) {
        return n;
    }
    else if (n % i == 0) {
        return i;
    }
    else {
        return LeastPrimeDivisor(n, i + 1);
    }
}

/*Функция IsPrime(n) возвращает true, если n - простое и
возвращает false, если n - составное*/

bool IsPrime(int n) 
{
    return LeastPrimeDivisor(n, 2) == n;
}

/*Функция PrintVector печатает вектор из элементов типа int*/

void PrintVector(const vector<int>& v) {
    for (const auto& a : v) {
        cout << a << " ";
    }
    cout << endl;
} 

int main() 
{
    int n;
    cin >> n;
    vector<int> primes; 
    for (int k = 2; primes.size() < abs(n); ++k) {
        if (IsPrime(k)) {
            primes.push_back(k);
        }
    }
    if (n < 0) {
        reverse(begin(primes), end(primes));
    }
    PrintVector(primes);
    return 0;
}