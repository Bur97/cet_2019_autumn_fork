#include <iostream>
#include <algorithm>
#include <cassert>

using namespace std;

class Frac {
private:
    int num;
    int den;
public:
    Frac() {
        num = 0;
        den = 1;
    }
    Frac(int num, int den) {
        assert(den);
        if (den < 0) {
            num *= -1;
            den *= -1;
        }
        if (num == 0) {
            this -> num = 0;
            this -> den = 1;
        }
        else {
            this -> num = num / __gcd(abs(num), abs(den));
            this -> den = den / __gcd(abs(num), abs(den));
        }
    }
    int Numerator() const {
        return num;
    }
    int Denominator() const {
        return den;
    }
    void print(ostream& out) {
        out << num << "/" << den << endl;
    }

    friend istream& operator>>(istream& in, Frac& f);
    friend ostream& operator<<(ostream& out, const Frac& f);
};

Frac operator+(const Frac& f, const Frac& g) {
    int num = f.Numerator()*g.Denominator() + g.Numerator()*f.Denominator();
    int den = f.Denominator()*g.Denominator();
    return Frac(num, den);
}

Frac operator-(const Frac& f, const Frac& g) {
    int num = f.Numerator()*g.Denominator() - g.Numerator()*f.Denominator();
    int den = f.Denominator()*g.Denominator();
    return Frac(num, den);    
}

Frac operator*(const Frac& f, const Frac& g) {
    int num = f.Numerator()*g.Numerator();
    int den = f.Denominator()*g.Denominator();
    return Frac(num, den);
}

Frac operator/(const Frac& f, const Frac& g) {
    assert(g.Numerator());
    int num = f.Numerator()*g.Denominator();
    int den = f.Denominator()*g.Numerator();
    return Frac(num, den);
}

bool operator>(const Frac&f, const Frac& g) {
    return (f - g).Numerator() > 0;
}

bool operator<=(const Frac& f, const Frac& g) {
    return (f - g).Numerator() <= 0;
}

bool operator==(const Frac& f, const Frac& g) {
    return (f - g).Numerator() == 0;
}

bool operator!=(const Frac& f, const Frac& g) {
    return (f - g).Numerator() != 0;
}

Frac& operator-(Frac& f) {
    f = Frac() - f;
    return f;
}

Frac& operator+=(Frac& f, const Frac& g) {
    f = f + g;
    return f;
}

Frac& operator-=(Frac& f, const Frac& g) {
    f = f - g;
    return f;
}

Frac& operator*=(Frac& f, const Frac& g) {
    f = f * g;
    return f;
}

Frac& operator/=(Frac& f, const Frac& g) {
    f = f / g;
    return f;
}

istream& operator>>(istream& in, Frac& f) {
    in >> f.num;
    in.ignore(1);
    in >> f.den;
    return in;
}

ostream& operator<<(ostream& out, const Frac& f) {
    out << f.num << "/" << f.den; 
}

int main() {

    Frac f;
    cin >> f;
    cout << f << endl;

    return 0;
}