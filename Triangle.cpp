#include <iostream>
#include <cmath>

using namespace std;

class Point {
public:
    double x;
    double y;

    Point(double x = 0, double y = 0) {
        this -> x = x;
        this -> y = y;
    }

    void print_coordinates() {
        cout << "(" << x << ", " << y << ")" << endl;
    }
};

Point operator+(const Point& p, const Point& q) {
    Point r(p.x + q.x, p.y + q.y);
    return r;
}

Point operator*(double a, const Point& p) {
    Point r(a*p.x, a*p.y);
    return r;
}

Point operator-(const Point& p, const Point& q) {
    return p + (-1)*q;
}

double get_distance(const Point& p, const Point& q) {
    Point r = p - q;
    double dist = sqrt(pow(r.x, 2) + pow(r.y, 2));
    return dist;
}

class Triangle {
public:
    Triangle (Point p, Point q, Point r) {
        if (triangle_inequality(p, q, r)) {
            A = p;
            B = q;
            C = r;
        }
        else {
            cout << "The triangle with vertices: ";
            cout << "(" << p.x << ", " << p.y << "), ";
            cout << "(" << q.x << ", " << q.y << "), ";
            cout << "(" << r.x << ", " << r.y << "), ";
            cout << "cannot exist" << endl;
            exit(0); 
        }
    }
    double get_perimeter() {
        return get_distance(A, B) + get_distance(B, C) + get_distance(C, A); 
    }
    double get_area() {
        double P = get_perimeter() / 2;
        return sqrt(P * (P - get_distance(A, B)) * (P - get_distance(B, C)) * (P - get_distance(C, A)));
    }
     
private:
    Point A;
    Point B;
    Point C;
    bool triangle_inequality(const Point& p, const Point& q, const Point& r) {
        double pq = get_distance(p, q);
        double qr = get_distance(q, r);
        double rp = get_distance(r, p);
        return (pq < qr + rp && qr < rp + pq && rp < pq + qr);
    }
};

int main() {

    Point p, q(0, 1), r(1, 0);
    Triangle t(p, q, r);
    cout << t.get_perimeter() << " " << t.get_area() << endl;

    return 0;
}